% !TEX root = beispielMA.tex
\section{Ergebnisse}
\label{sec:ergebnisse-5-seiten}

\subsection{Algorithmen}
\label{sec:versch-algor-versch}


\subsubsection{Middle-Square-Methode}
\label{sec:middle-square-meth}
Die Middle-Square-Methode wurde von Johann \textsc{von Neumann} 1949
vorgestellt und ist eine der ältesten Methoden zur Generierung von
Zufallszahlen \citep[vgl.][S.
9]{Baumeister2011}. Die Middle-Square-Methode verläuft nach dem
folgenden Schema:

\begin{figure}[h]
  \centering
  \input{Bilder/middle-square-fluss.tex} % Flussdiagramm einfügen
  \caption{Middle-Square-Methode als Flussdiagramm}
  \label{fig:middle-square-flussdiagramm}
\end{figure}

Da es nur endlich viele vierstellige Zahlen gibt und der
Middle-Square-Generator rekursiv ist, wird bei jedem Startwert
nach absehbarer Zeit eine Periode eintreffen. Wählt man beispielsweise den
Startwert 8841, erhält man die Folge
\begin{equation*}
8841, 2504, 2700, 2900, \underbrace{4100, 8100, 6100, 2100},
\underbrace{4100, 8100, \dots} 
\end{equation*}  
Es stellt sich also die Frage nach dem besten Anfangswert. Mit dem
Programm aus Anhang \vref{sec:middle-square} wird getestet, nach wie
vielen Zahlen eine Zahl zum zweiten Mal erscheint. Die beste
Anfangszahl ist 6239, die Folge verfällt erst nach 110 Zahlen in eine
Periodizität. Allerdings besitzt diese Periode nur die kurze
Länge 4. Als Arbeitshypothese wäre jedoch durchaus denkbar, dass eine
Folge mit einem anderen Startwert zwar früher in eine Periode fällt,
diese dafür länger wäre. 

Um diese Eigenschaft zu testen, zählt das selbe Programm auch die
Periodenlängen und gibt den Rekordwert aus. Erstaunlicherweise ist mit
keinem Startwert eine Periode von mehr als 4 Gliedern zu generieren.

Der Middle-Square-Generator kann bereits deswegen nicht als
\enquote{guter} Generator gelten, es sollen aber die ersten 110 Zahlen
der Folge mit Startwert 6239 untersucht werden.

Als erstes wird die Folge grafisch analysiert. In der Abbildung
\vref{fig:midsquaure} ist die Zahlenfolge mit Startwert 6239
dargestellt. Um sie in das Intervall [0,1] zu transformieren, wurden
die Zahlen durch $10\,000$ dividiert.
\begin{figure}[h]
  \centering
\includegraphics[width=0.75\textwidth]{Bilder/middlesquare.pdf}
  \caption{Middle-Square-Zahlenfolge mit Startwert 6239 dividiert
    durch $10\, 000$: Zufallszahl vs. Ordnungszahl, $n$ = 110}
  \label{fig:midsquaure}
\end{figure}

Aufgrund der kleinen Stichprobengrösse können keine
Aussagen zu möglichen Mustern gemacht werden.

Als nächstes wird das statistische Verfahren zur Überprüfung der
Zufälligkeit angewendet.

\begin{table}[!b] \centering
\begin{tabular}{|l|c|c|}
\hline
$n$ & \multicolumn{2}{c|}{110}\\ \hline
$\overline{x}$ & \multicolumn{2}{c|}{0.5181}\\ \hline
$s$ & \multicolumn{2}{c|}{0.2841}\\
\hline
\hline
% $\alpha$ & \multicolumn{2}{c|}{0.5}\\
% \hline
% \hline
& Ja & Nein\\ \hline
1) $t$-Test ($\mu$, $\overline{x}$) & $\times$ & \\ \hline 
 2) $t$-Test ($\overline{x_1}, \overline{x_2}$) & $\times$ & \\ \hline
 3) $F$-Test &$\times$ & \\ \hline % bis hier bestanden
 4) Häufigkeiten & - & -  \\ \hline
 5) $\chi^2$-Test &$\times$ & \\ \hline
\hline
 \multicolumn{3}{|c|}{$H_0$: Annahme}\\
\hline

\end{tabular}
\caption{Statistik: Middlesquare mit Startwert 6239 (ohne Periode), Tests mit
  Fehlerniveau $\alpha=0.05$}
  \label{tab:hyp-misq}
\end{table}

Der $t$- und der $F$-Test vergleichen zwei Stichproben miteinander. Dafür
wurde eine zweite Middle-Square-Zahlenfolge mit dem Startwert 3416
gewählt. Diese fällt nach 108 Zahlen in eine Periode und ist nach
diesem Kriterium die \enquote{zweitbeste} Zahlenfolge. Weiter ergibt ein Vergleich der Häufigkeiten mit Erwartungswerten bei der
Auswertung einer Stichprobe, in der ein Wert \textit{nie} zwei Mal
erscheint, keinen Sinn. Jeder Wert erscheint höchstens einmal, dies
ist für eine Zahlenfolge zwischen 0 und 10\,000 mit 110 Werten zu
erwarten. Der vierte Test wurde darum nicht durchgeführt. Alle
anderen Tests wurden bestanden (vgl.\,Tabelle~\vref{tab:hyp-misq}).
Die Nullhypothese $H_0$ über eine zufällige Verteilung kann deshalb zu einem
Fehlerniveau von $\alpha=0.05$ nicht abgelehnt werden. Es ist jedoch klar, dass mit Einbezug der Periode
sämtliche Tests bei grösser werdender Stichprobe nicht bestanden
werden, da die Regel der Unvorhersehbarkeit verletzt wird.


\subsubsection{Linearer Kongruenz-Generator}
\label{sec:line-kongr-gener}

Ein linearer Kongruenz-Generator verknüpft eine rekursive Darstellung
mit Modulorechnung. Er besitzt die allgemeine Form 
\begin{equation}
  \label{eq:15}
  x_{n+1}=(ax_n+c)\mod m, \qquad x_1, a, m \in \mathbb{N}, \quad c
  \in \mathbb{N}_0.
\end{equation}
Dabei können $a,c$ und $m$ frei gewählt werden. Linear kongruente
Generatoren werden in der Literatur häufig
verwendet, darum soll hier ein bewährtes Beispiel aus
\cite{LEcuyer1999} zitiert nach 
\citep[][S. 38]{Kolonko2008} auf seine Zufälligkeit hin geprüft
werden. Der Generator MRG32k3a verbindet zwei lineare
Kongruenzgeneratoren zu einem:
\begin{align}
  \label{eq:16}
    x_{n+1} & =(1403580 \cdot x_{n-1} - 810728 \cdot x_{n-2})\mod (2^{32}
    - 209),&x_{1}=x_2=x_3=1 \\
    y_{n+1} &= (527612 \cdot y_{n} - 1370589 \cdot y_{n-2})\mod
    (2^{32}-22853), &y_1=y_2=y_3=1\\
    z_{n-2} &= (x_{n+1} - y_{n+1})\mod (2^{32}-209),\qquad \qquad \qquad  n \geq 3,
&     z_{n} \in
       \mathbb{Z}\cap [0,2^{32}-210[
\end{align}

Eine weitere Besonderheit dieses Generators ist, dass er $x_{n-1}$ und
$x_{n-2}$ anstelle von $x_n$ verwendet. Ähnlich verhält es sich mit
$y_n$: Nur $y_n$ und $y_{n-2}$ kommen vor. So werden Elemente
übersprungen. Um die Zahlen $z_n$ in das
Intervall $[0,1[$ zu transformieren, werden sie anschliessend noch
durch $2^{32}-209$ dividiert. 

Auch hier wird wieder zuerst die grafische Darstellung in Abbildung
\vref{fig:MRG23k3a} betrachtet:

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{Bilder/MRG32k3a.pdf}
  \caption{MRG32k3a: Zufallszahl vs. Ordnungszahl, $n = 20\,000$, Zahlen
    gerundet auf 16. Nachkommastelle}
  \label{fig:MRG23k3a}
 \end{figure}

Im Gegensatz zur Grafik der Middle-Square-Methode ist die hier abgebildete
Stichprobengrösse wesentlich grösser. Es ist festzustellen, dass keine
Muster, Senken oder Anhäufungen ersichtlich sind.

\begin{table}[h] \centering
\begin{tabular}{|l|c|c|} 
\hline
$n$ & \multicolumn{2}{c|}{$10^6$}\\ \hline
$\overline{x}$ & \multicolumn{2}{c|}{0.49998}\\ \hline
$s$ & \multicolumn{2}{c|}{0.28849}\\
\hline
\hline & Ja & Nein\\ \hline
1) $t$-Test ($\mu$, $\overline{x}$) & $\times$ & \\ \hline 
 2) $t$-Test ($\overline{x_1}, \overline{x_2}$) & $\times$ & \\ \hline
 3) $F$-Test &$\times$ & \\ \hline
 4) Häufigkeiten & & $\times$\\ \hline % Nicht bestanden!
 5) $\chi^2$-Test & $\times$& \\ \hline
\hline                               
 \multicolumn{3}{|c|}{$H_0$: Rückweisung}\\
\hline

\end{tabular}
\caption{Statistik: MRG32k3a, Tests mit Fehlerniveau $\alpha = 0.05$}
  \label{tab:hyp-lkg}
\end{table}

Wie in Tabelle~\vref{tab:hyp-lkg} ersichtlich ist, wurden alle Tests
bis auf den vierten Test, welcher theoretische Häufigkeit und empirische
Häufigkeit vergleicht, bestanden. Die Nullhypothese $H_0$ über eine
zufällige Verteilung der generierten Zahlen wird deshalb mit der
Irrtumswahrscheinlichkeit von 5\% zurückgewiesen. 




\subsubsection{$\pi$-Zeit-Generator}
\label{sec:pi-zeit-generator}
Der $\pi$-Zeit-Generator ist ein eigenständiger Versuch, Zufallszahlen zu
produzieren. Die Idee dahinter ist, abhängig von der Zeit eine
bestimmte Stelle von $\pi$ auszuwählen. Um eine häufig wechselnde Ziffer
zu erhalten, werden die sechste und siebte Nachkommastelle der
gemessenen Zeit in Sekunden genommen. Im Bereich der Mikrosekunden ist Python 
nicht mehr exakt, deshalb sind bei gleicher Programmdauer
unterschiedliche Zahlen zu erwarten. Abhängig von dieser Zahl wird die Ziffer an dieser Stelle von $\pi$ bestimmt. Dieser
Vorgang wird viermal durchgeführt, um eine vierstellige Ziffer zu
erhalten. Der $\pi$-Zeit-Generator ist in
Abbildung~\vref{fig:pi-zeit-fluss} als Flussdiagramm verdeutlicht.

\begin{figure}[ph]
\centering  

  $n$-maliges Aufrufen:

  \bigskip

  \input{Bilder/pi-zeit.tex}
  \caption{$\pi$-Zeit-Generator als Flussdiagramm}
  \label{fig:pi-zeit-fluss}
\end{figure}

Im Gegensatz zu den oben vorgestellten Generatoren wird hier
keine Rekursion verwendet, d.\,h.\,die Gefahr der Periode kann so
umgangen werden. Die Nachteile liegen
 vor allem in der grösseren
Rechenleistung.

   \begin{figure}[ph]
     \centering
     \includegraphics[width=0.75\textwidth]{Bilder/pi-zeit-zahlen.pdf}
     \caption{$\pi$-Zeit-Generator: Zufallszahl vs. Ordnungszahl, $n =
       20\,000$}
     \label{fig:pize
it-grafik}
   \end{figure}

In Abbildung~\vref{fig:pize
it-grafik} sind eindeutig horizontale Linien in gleichmässigen
Abständen zu sehen. Dass sich die Punkte auf gleichmässigen
Gitterlinien befinden, ist nicht weiter erstaunlich. Es gibt nämlich
nur 10\,000 mögliche Werte, die dieser Generator ausgeben kann. Wenn
nun 20\,000 Punkte abgebildet werden, muss es zwangsläufig zu
Mehrfachbelegung eines Wertes kommen. Gegen diese Begründung ist
jedoch einzuwenden, dass die Mehrfachbelegung \textit{zufällig}
zustande kommen müsste und es deshalb keine Konzentration geben sollte. 

Eine mögliche Fehlerquelle dieses Algorithmus ist die
ungleichmässige Verteilung der ausgewählten Ziffern aus der Zeit. Sind
bereits diese nicht zufällig, werden die Endwerte auch nicht zufällig
sein. Die Abbildung~\vref{fig:zif-pi-zeit} zeigt deutlich, dass
bereits bei der Auswahl der Ziffern keine Zufälligkeit vorhanden ist,
es bilden sich Muster.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{Bilder/ziffern-pi-zeit.pdf}
  \caption{ausgewählte Ziffern aus dem Python Zeit-Modul, $n = 20\,000$ zur
    Generierung von $5\,000$ Zufallszahlen}
  \label{fig:zif-pi-zeit}
\end{figure}

Die Schlussfolgerung aus dieser Grafik ist, dass Python entgegen der
Annahme für solche
Zwecke zu exakt arbeitet. Da Zufälligkeit bereits aufgrund dieser beiden
Darstellungen ausgeschlossen werden muss, wird die Nullhypothese $H_0$ über
eine zufällige Verteilung bereits jetzt abgelehnt und auf
weitere Analysen verzichtet.

\subsection{Physische Zufallsgeneratoren}
\label{sec:zufallsz-aus-dem}

\subsubsection{Swisslotto}
\label{sec:swisslotto}

Die grafische Auswertung nach Abbildung~\vref{fig:lotto} ist in diesem
Fall nicht sehr erkenntnisreich, da die Zahl 0 deutlich häufiger vorkommen
\textit{muss}. Diese Methode ist hier nicht geeignet, weil
nicht einzuschätzen ist, ob eine Häufung dem Verhältnis 7:38
entspricht. Es kann hier weder eine Linienbildung noch eine
ungleichmässige Verteilung bewertet werden. 

\begin{figure}[h]
  \centering
 \includegraphics[width=\textwidth]{Bilder/lotto-25-v3.pdf}
  \caption{Ziehung der Zahl 25, die Zahl 1 entspricht einer Ziehung, 0
    einer Nicht-Ziehung, $n = 1\,000$}
  \label{fig:lotto}
\end{figure}

Für den ersten $t$-Test wurde $\mu$ nach Gleichung~(\ref{eq:18})
\vpageref{eq:18} verwendet, für den zweiten $t$-Test und den $F$-Test
wurden die beiden Hälften der Stichprobe miteinander verglichen. Der
Häufigkeitstest sowie der $\chi^2$-Test konnte nicht durchgeführt
werden, da beim ersteren zu wenig Daten vorhanden sind und beim
letzteren nur zwei Klassen gebildet werden können. Dies entspräche
wiederum dem Häufigkeitstest. Im Grunde genommen prüft schon der erste
$t$-Test auf eine Verteilung hin, da $\mu$ der erwarteten Häufigkeit
des Wertes 1 (d.\,h.\, die Zahl 25 wurde gezogen) dividiert durch die Stichprobengrösse entspricht.

Die Tabelle~\vref{tab:hyp-25} zeigt, dass die Hypothese $H_0$ über
eine zufällige Ziehung der Zahl 25 nicht verworfen werden kann und die
Nullhypothese $H_0$ aus diesem Grund zu einem Fehlerniveau von 5\% angenommen wird.

 \begin{table}[h]
   \centering
 \begin{tabular}{|l|c|c|}
\hline
$n$ & \multicolumn{2}{c|}{1\,858}\\ \hline
$\overline{x}$ & \multicolumn{2}{c|}{0.1469}\\ \hline
$s$ & \multicolumn{2}{c|}{0.35413}\\
\hline
 \hline
  & Ja & Nein\\ \hline
1) $t$-Test ($\mu$, $\overline{x}$) & $\times$ & \\ \hline
 2) $t$-Test ($\overline{x_1}, \overline{x_2}$) &  $\times$ &\\ \hline
 3) $F$-Test &$\times$  & \\ \hline
 4)   Häufigkeiten & -  & -\\ \hline % zu wenig Daten
 5)  $\chi^2$-Test & - & - \\ \hline % nur zwei Klassen, das selbe wie
\hline                                % 4)
 \multicolumn{3}{|c|}{$H_0$: Annahme}\\
\hline
 \end{tabular}
   \caption{Statistik: Ziehung der Zahl 25, Tests mit
  Fehlerniveau $\alpha=0.05$}
   \label{tab:hyp-25}
 \end{table}

\subsubsection{Radioaktiver Zerfall}
\label{sec:radioaktiver-zerfall}
Die Abbildung~\vref{fig:radakt} zeigt die Anzahl Impulse pro 10 ms. Es
ist auffällig, dass sich die meisten Messpunkte zwischen 10 und 20
befinden. Ausserdem bilden sich Linien, dies ist aber nicht weiter
erstaunlich, da die gemessenen Werte diskret sind und sich im
Wesentlichen zwischen ungefähr zehn Zahlen bewegen. 

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{Bilder/radaktiv.pdf}
  \caption{Zerfall von $^{241}$Am, Zeiteinheit: 10 ms, Abstand zur Sonde $\approx$ 1.5cm, $n= 1\,000$}
  \label{fig:radakt}
\end{figure}

Das Histogramm mit eingezeichnetem Verlauf der Normalverteilung (siehe
Abbildung~\vref{fig:hist-radakt}) zeigt, dass die Zahl der Impulse
pro 10 ms annähernd normalverteilt sind. Nach \citep[][S.
661]{Gellert1974} entsprechen sie einer Poissonverteilung. Diese
nähert sich für grosse $\mu$ einer Normalverteilung an. Diese
Beobachtung ist deshalb wichtig, weil die theoretische
Wahrscheinlichkeit für die Häufigkeit eines Wertes im Häufigkeits- und
$\chi^2$-Test nach einer Poissonverteilung berechnet werden muss.


Die ersten vier Tests (vgl.\,Tabelle~\vref{tab:radakt}) werden
bestanden. Für den ersten und den vierten Tests wurde der Mittelwert
der ersten $2\,000$ Zahlen mit dem gesamten Mittelwert, für zweiten
und dritten Tests je eine Hälfte mit der anderen verglichen. Der
$\chi^2$-Test wurde nicht bestanden, deshalb wird die Nullhypothese
$H_0$ einer zufälligen Poisson-Verteilung verworfen.
% Die Abbildung~\vref{fig:radaktlin} zeigt die kumulierte zu erwartende
% Häufigkeit (rote Gerade) verglichen mit der tatsächlichen Häufigkeit
% (blau). Die Skala ist so gewählt, dass bei einer Normalverteilung die
% kumulierte Wahrscheinlichkeit für einen Wert linear zur $x$-Achse
% wird. Es ist ersichtlich, dass vor allem die Werte zwischen 0-5 sowie
% ab 23 deutlich von der idealisierten Gerade abweichen.

%     \begin{figure}[h]
%       \centering
%  \include
%  graphics[width=0.8\textwidth]{Bilder/probabilityofradioaktiveimpulse.pdf} %% % Achtung Grafik enthält Schreibfehler!
%  \caption{Radioaktive Impulse: Erwartete vs. empirische Häufigkeit, $y$-Achse skaliert}
%       \label{fig:radaktlin}
%     \end{figure}


  \begin{table}[th]

   \centering
 \begin{tabular}{|l|c|c|}
\hline
$n$ & \multicolumn{2}{c|}{60\,000}\\ \hline
$\overline{x}$ & \multicolumn{2}{c|}{13.461}\\ \hline
$s$ & \multicolumn{2}{c|}{2.996}\\
\hline
 \hline
  & Ja & Nein\\ \hline
1) $t$-Test ($\mu$, $\overline{x}$)&$\times$&\\ \hline
 2) $t$-Test ($\overline{x_1}, \overline{x_2}$)&$\times$& \\ \hline
 3) $F$-Test& $\times$ &\\ \hline % bis hier  bestanden
 4)   Häufigkeiten & $\times$ &\\ \hline
 5)  $\chi^2$-Test &  & $\times$\\ \hline
\hline                               
 \multicolumn{3}{|c|}{$H_0$: Rückweisung}\\
\hline

 \end{tabular}
   \caption{Statistik: Radioaktiver Zerfall, Tests mit
  Fehlerniveau $\alpha=0.05$}
   \label{tab:radakt}
 \end{table}

\begin{figure}[p]
  \centering
\includegraphics[width=0.9\textwidth]{Bilder/Histogramm-radioaktiv}
  \caption{Histogramm der radioaktiven Impulse, Normalverteilung eingezeichnet}
  \label{fig:hist-radakt}
\end{figure}

\subsubsection{Kaugummi}
\label{sec:kaugummi}
Die Abbildung~\vref{fig:kaugummi} wurde für die Auswertung ausgewählt
und die Koordinaten der einzelnen Kaugummis bestimmt. Als zu prüfende
Zufallszahl wurde jeweils die $y$-Koordinate gewählt.

Da hier eine grafische Verteilung statistisch überprüft werden soll, erübrigt
sich ein weiteres Diagramm.

\begin{figure}[p]
  \centering
  \includegraphics[width=\textwidth]{Bilder/kaugummi-neu-und-richtig}
  \caption{Verteilung von Kaugummis (farbig markiert), $n = 341$}
  \label{fig:kaugummi}
\end{figure}

Auf den ersten Blick lassen sich hier keine Muster erkennen.
Eine statistische Auswertung ergibt aber, dass nur zwei
der fünf Tests bestanden werden (vgl.\,Tabelle~\vref{tab:kaugummi})
. Für den zweiten $t$-Test und den $F$-Test wurde
jeweils eine Hälfte der Stichprobe mit der anderen verglichen.

Die Nullhypothese $H_0$ über eine zufällige Verteilung der Kaugummis wird
deshalb mit einer Irrtumswahrscheinlichkeit von 5\% abgelehnt.


 \begin{table}[h]
   \centering
 \begin{tabular}{|l|c|c|}
\hline
$n$ & \multicolumn{2}{c|}{305}\\ \hline
$\overline{x}$ & \multicolumn{2}{c|}{0.5207}\\ \hline
$s$ & \multicolumn{2}{c|}{0.3006}\\
\hline
 \hline
  & Ja & Nein\\ \hline
 1) $t$-Test ($\mu$, $\overline{x}$) & $\times$&\\ \hline
 2) $t$-Test ($\overline{x_1}, \overline{x_2}$)&$\times$& \\ \hline
 3) $F$-Test &$\times$ &\\ \hline
 4)   Häufigkeiten& &$\times$  \\ \hline  %nicht bestanden
 5)  $\chi^2$-Test &  & $\times$\\ \hline  %nicht bestanden
\hline                               
 \multicolumn{3}{|c|}{$H_0$: Rückweisung}\\
\hline
 \end{tabular}
   \caption{Statistik: Kaugummi, Tests mit
  Fehlerniveau $\alpha=0.05$}
   \label{tab:kaugummi}
 \end{table}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Bericht"
%%% End: 
