import mymodul

# initialisieren
liste = []
zaehler = 0
periode = 0

 # Datei "rekord.txt" mit 0 überschreiben
rekordnull = open("rekord.txt", "w")
rekordnull.write(str(0))
rekordnull.close()

 # Datei "periodemax.txt" mit 0 überschreiben
periodenull = open("periodemax.txt", "w") 
periodenull.write(str(0))
periodenull.close()

def generator(startwert): # eigentlicher Generator
    # falls nicht 8 Stellen mit Nullen auffüllen:
    neuwert = str("00000000" + str(startwert*startwert))[-6:-2]
    return neuwert # neue Zahl ausgeben

def rekord(anzahl, zahlenfolge, startwert): 
    # auf Rekord befor Periodizität überprüfen:
    rekordbisher = open("rekord.txt")
    if int(rekordbisher.readline()) < anzahl:
        rekordbisher.close()

        # Neuen Rekordwert eintragen:
        rekordneu = open("rekord.txt", "w") 
        rekordneu.write(str(anzahl) + "\n" + \
                        "Startwert:" + str(startwert))
        rekordneu.close()

        # Zahlenfolge schreiben:
        mymodul.csv(zahlenfolge, "zufallsfolge.txt")

def perioderek(periode, zahlenfolge, startwert):
    # auf Rekord Periodenlänge überprüfen:
    rekordbisher = open("periodemax.txt")
    if int(rekordbisher.readline()) < periode:
        rekordbisher.close()

         # Neuen Rekordwert eintragen
        rekordneu = open("periodemax", "w")
        rekordneu.write(str(periode) + "\n" + \
 "Startwert:" + str(startwert))
        rekordneu.close()
        # Datei schreiben
        mymodul.csv(zahlenfolge, "periodenfolge.txt") 


#alle Anfangswert mit 4 Ziffern überprüfen :
for i in range(1000,10001):
    wert = int(generator(i)) # erste Zufallszahl bestimmen
    liste.append(wert) # Wert zu Liste hinzufügen
    while not periode: # Solange keine Periodizität
        liste.append(generator(wert))
        # Ein Wert zweimal in der Liste?
        if liste.count(generator(wert)) >= 2:
            periode = 1 # Wenn ja, dann Periode

        wert = int(generator(wert)) # Neuen Anfangswert bestimmen
        zaehler = zaehler + 1

    else: 
        #Periodenlänge bestimmen:
        laenge = len(liste) -1 - liste.index(liste[-1])
        #Funktion zur Überprüfung aufrufen:
        perioderek(laenge, liste, i) 
        zaehler= zaehler-1
        # Funktion überprüft auf Rekordlänge vor Periodizität:
        rekord(zaehler, liste[:-1], i) 
        # sämtliche Variablen leeren:
        zaehler = 0
        liste = []
        periode = 0

