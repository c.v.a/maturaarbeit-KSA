# Beide Funktionen schreiben Liste inklusiv Erscheinungszahlen in eine .csv-Datei.
def csv(liste, name): # Die Zahl wird jeweils an "0." angehängt.
    datei = open(name, "w")
    for item in range(len(liste)):
        datei.write(str(item + 1) + "; 0." + str(liste[item]) + "\n")
    datei.close()

def csv_lkg(liste, name):#Die Zahl wird nicht an "0." angehängt.
    datei = open(name, "w")
    for item in range(len(liste)):
        datei.write(str(item + 1) + "; " + str(liste[item]) + "\n")
    datei.close()
